# SWT Paperclips

Fork of http://www.eclipse.org/nebula/widgets/paperclips/paperclips.php

## Usage

Gradle
```groovy
repositories {
    maven {  url "https://michaelsp.github.io/maven-repo" }
}

dependency {
    implementation "com.github.michaelsp:swt-paperclips:1.5"
}
```

Maven
```xml
<repositories>
    <repository>
        <id>michaelsp</id>
        <url>https://michaelsp.github.io/maven-repo</url>
    </repository>
</repositories>

<dependencies>
    <dependency>
      <groupId>com.github.michaelsp</groupId>
      <artifactId>swt-paperclips</artifactId>
      <version>1.5</version>
    </dependency>
</dependencies>
```